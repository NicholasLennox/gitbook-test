---
description: Lesson description
---

# Lesson 1

Lesson content

| Col 1 | Col 2  | Col 3 |
| ----- | ------ | ----- |
| Row 1 | Row 1  | Row 1 |
| Row 2 | Row 2  | Row 2 |
| Row 3 | Row 3  | Row 3 |

* [x] Checklist item 1
* [ ] Checklist item 2

> Quote

{% code title="index.html" overflow="wrap" lineNumbers="true" %}
```html
<span>Html code block</span>
```
{% endcode %}

```csharp
Console.WriteLine("Hello World");
```

<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td></td><td>Card 1</td><td></td></tr><tr><td></td><td>Card 2</td><td></td></tr><tr><td></td><td>Card 3</td><td></td></tr></tbody></table>

{% tabs %}
{% tab title="First Tab" %}
```
// Some code
```
{% endtab %}

{% tab title="Second Tab" %}
```
// Some code
```
{% endtab %}
{% endtabs %}

<details>

<summary>Expand title</summary>

Expand content

</details>

{% hint style="info" %}
Info
{% endhint %}

{% hint style="warning" %}
Warning
{% endhint %}

{% hint style="danger" %}
Danger
{% endhint %}

{% hint style="success" %}
Success
{% endhint %}
